package com.spring.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.entity.User;

@RestController
public class UserController {
	
	static List<User> allUser = new ArrayList<>();
	static {
		
		User u1 = new User(1L, "ABC", "Hyderabad", 25);
		User u2 = new User(2L, "XYZ", "Chennai", 30);
		User u3 = new User(3L, "PQR", "Mumbai", 22);
		
		allUser.add(u1);
		allUser.add(u2);
		allUser.add(u3);
		
		
	}
	@GetMapping("/users")
	public List<User> getAllUser() {
		return allUser;
	}
	@GetMapping("/users/{id}")
	public User getOneUser(@PathVariable("id") Long id) {
		
		
		for(User u : allUser) {
			if(u.getId() == id) {
				return u;
			}
		}
		return null;
	}
	@PostMapping("/users")
	public User addUser(@RequestBody User requestUser) {
		
		allUser.add(requestUser);
		return requestUser;	
	}
	@PutMapping("/users/{id}")
	public User updateUser(@PathVariable("id") Long id, @RequestBody  User newUserValue)
	{
		User dbUser = null;
		int index = 0;
		for(User u : allUser) {
			if(u.getId() == id) {
				dbUser = u;
				index = allUser.indexOf(dbUser);
				System.out.println("index of deleted user:"+index);
				dbUser.setName(newUserValue.getName());
				dbUser.setCity(newUserValue.getCity());
				dbUser.setAge(newUserValue.getAge());
			}
		}
		
		if(dbUser != null)
		{
			allUser.remove(index);
			allUser.add(dbUser);
			
		}
		return dbUser;
	}	
	
	
	
	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable("id") Long id) {
		for(User u : allUser) {
			if(u.getId() == id) {
				  allUser.indexOf(allUser.indexOf(u));
				
			}
		}
		return "User Deleted succcessfully.";
	
	}
}
