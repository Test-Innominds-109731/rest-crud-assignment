package com.spring.rest.entity;

public class User {

	
	private Long id;
	private String name;
	private String city;
	private Integer age;
	
	
	public User(Long id, String name, String city, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.age = age;
	}
	
	
	public User() {
		super();
		
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	
	
}
