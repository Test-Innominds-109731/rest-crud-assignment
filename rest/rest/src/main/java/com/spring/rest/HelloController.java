package com.spring.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	
	//@RequestMapping(value="/hello", method = RequestMethod.GET)
	@GetMapping("/hello")
	public String HelloRestApi() {
		return "Hello World!!";
	}
	
	
	@RequestMapping(value="/helloPost", method = RequestMethod.POST)
	public String HelloRestApiPost() {
		return "Hello Post";
	}
	
	@RequestMapping(value={"/find", "/search*"}, method = RequestMethod.GET)
	public String findAllUsers() {
		return "GettingUsers";
	}
	
	@RequestMapping(value="/pathparamexample/{value}", method = RequestMethod.GET)
	public ResponseEntity<String> getPathParam(@PathVariable String value) { 
		System.out.println("Path Variable Value: "+value);
		return new ResponseEntity<String>(value,HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/requestparamexample", method = RequestMethod.GET)
	public ResponseEntity<String> getRequestParmParam(@RequestParam("value") String value) { 
		System.out.println("Request Param Value: "+value);
		return new ResponseEntity<String>(value,HttpStatus.OK);
	}
	
	/*
	 * 
	 * HTTP method-specific shortcut
Spring introduces annotation for different HTTP methods. The varients of @RequestMapping are @GetMapping, @PostMapping, @PutMapping, @DeleteMapping and @PatchMapping annotations with sample code examples.

@GetMapping — shortcut for @RequestMapping(method = RequestMethod.GET)
@PostMapping — shortcut for @RequestMapping(method = RequestMethod.POST)
@PutMapping — shortcut for @RequestMapping(method = RequestMethod.PUT)
@DeleteMapping — shortcut for @RequestMapping(method =RequestMethod.DELETE)
@PatchMapping — shortcut for @RequestMapping(method = RequestMethod.PATCH)*/
}
